// initialize the map, add context menu
var map = L.map('map',
	{
		contextmenu: true,
		contextmenuItems: [{
			text: 'Koordinaten anzeigen',
			callback: showCoordinates
		}, {
			text: 'Karte hier zentrieren',
			callback: centerMap
		}, '-', {
			text: 'Zoom in',
			icon: '/images/zoom_in.png',
			callback: zoomIn
		}, {
			text: 'Zoom out',
			icon: '/images/zoom_out.png',
			callback: zoomOut
		}]
	}
);

// zoom to location if given, default otherwise
if (url('?latitude') && url('?longitude')) {
	map.setView([url('?latitude'), url('?longitude')], 16)
} else {
	// (center to Berlin)
	map.setView([52.514967, 13.378601], 12)
	// zoom to location, if allowed (if not stays in default location)
	map.locate({setView: true, maxZoom: 15});
}

// load tile layer via plugin
var layerMaps = {};
// loading both layers was buggy, topomap always shined through, fix this later
//layerMaps["OpenTopoMap"] = L.tileLayer.provider('OpenTopoMap').addTo(map);
layerMaps["OpenStreetMap"] = L.tileLayer.provider('OpenStreetMap.Mapnik').addTo(map);

// marker definitions
var markerMovieDarkBlue = L.icon(
	{
		iconUrl: '/images/marker_movie_darkblue.png',
		iconSize: [30,45],
		iconAnchor: [15,45]
	}
);
var markerBookDarkGreen = L.icon(
	{
		iconUrl: '/images/marker_book_darkgreen.png',
		iconSize: [30,45],
		iconAnchor: [15,45]
	}
);

// load data (asynchronously)
var language = "de";
var movieLoad = $.getJSON(`/data/movies_${language}.json?v=1`);
var bookLoad = $.getJSON(`/data/books_${language}.json`);

$.when(movieLoad, bookLoad).done(
	function(movieData, bookData) {

		var movies = L.geoJSON(
			movieData,
			{
				pointToLayer: function(feature,latlng) {
					var marker = L.marker(latlng, {icon: markerMovieDarkBlue});
					marker.bindPopup(`<div class="card" style="width: 18rem;">
						<div class="card-body">
							<h5 class="card-title">${feature.properties.location_title}</h5>
							<p class="card-text">aus: ${feature.properties.media_title}<br />${feature.properties.media_location_description}</p>
							<a href="/movies/movie_${feature.properties.media_id}_${language}.html#${feature.properties.location_id}" class="btn btn-outline-primary"><i class="fa fa-film"></i>&nbsp;&nbsp;Details</a>
						</div>`
					);
					return marker;
				}
			}
		);
		var movieCluster = L.markerClusterGroup();
		movieCluster.addLayer(movies);
		map.addLayer(movieCluster);

		var books = L.geoJSON(
			bookData,
			{
				pointToLayer: function(feature,latlng) {
					var marker = L.marker(latlng, {icon: markerBookDarkGreen});
					marker.bindPopup(`<div class="card" style="width: 18rem;">
						<div class="card-body">
							<h5 class="card-title">${feature.properties.location_title}</h5>
							<p class="card-text">aus: ${feature.properties.media_title}<br />${feature.properties.media_location_description}</p>
							<a href="/books/book_${feature.properties.media_id}_${language}.html#${feature.properties.location_id}" class="btn btn-outline-primary"><i class="fa fa-film"></i>&nbsp;&nbsp;Details</a>
						</div>`
					);
					return marker;
				}
			}
		);
		var bookCluster = L.markerClusterGroup();
		bookCluster.addLayer(books);
		map.addLayer(bookCluster);

		// layer control: layer selector
		// this call needs to be here, because only here all data is loaded
		L.control.layers(layerMaps, {"Filme": movieCluster, "Bücher": bookCluster}).addTo(map);

	}
);

// layer control: scale
L.control.scale().addTo(map);

// functions for context menu
function showCoordinates (e) {
	prompt("Koordinaten (zum Kopieren)", e.latlng);
}

function centerMap (e) {
	map.panTo(e.latlng);
}

function zoomIn (e) {
	map.zoomIn();
}

function zoomOut (e) {
	map.zoomOut();
}
