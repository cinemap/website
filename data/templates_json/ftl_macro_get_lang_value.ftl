<#macro get_lang_value lang, lang_default, map_content={}><#compress>
  <#if (map_content?keys?size > 0)>
    <#if map_content[lang]??>
      ${map_content[lang]}
    <#else>
      <#if map_content[lang_default]??>
        ${map_content[lang_default]}
      <#else>
        ${map_content[map_content?keys?first]}
      </#if>
    </#if>
  </#if>
</#compress></#macro>
