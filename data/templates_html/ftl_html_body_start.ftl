<body>

	<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
		<a class="navbar-brand" href="/">Karte</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor02" aria-controls="navbarColor02" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>

		<div class="collapse navbar-collapse" id="navbarColor02">
			<ul class="navbar-nav ml-auto">
				<li class="nav-item<#if (active_nav == "movies") > active</#if>">
					<a class="nav-link" href="/movies_${lang}.html">Filme</a>
				</li>
				<li class="nav-item<#if (active_nav == "books") > active</#if>">
					<a class="nav-link" href="/books_${lang}.html">Bücher</a>
				</li>
				<li class="nav-item<#if (active_nav == "participate") > active</#if>">
					<a class="nav-link" href="/participate_${lang}.html">Mitmachen</a>
				</li>
				<li class="nav-item<#if (active_nav == "imprint") > active</#if>">
					<a class="nav-link" href="/imprint_${lang}.html">Impressum</a>
				</li>
				<!--li class="nav-item dropdown">
					<a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" id="dropdown1">Dropdown</a>
					<div class="dropdown-menu" aria-labelledby="dropdown1">
						<a class="dropdown-item" href="#">eins</a>
						<div class="dropdown-divider"></div>
						<a class="dropdown-item" href="#">zwei</a>
						<a class="dropdown-item" href="#">drei</a>
					</div>
				</li-->
			</ul>
			<form class="form-inline ml-4 my-2 my-lg-0" action="/search.html" id="sitesearch" method="get" accept-charset="utf-8">
				<input class="form-control mr-sm-2" name="searchterm" type="text" placeholder="Suchtext">
				<button class="btn btn-secondary my-2 my-sm-0" type="submit">Suche</button>
			</form>
		</div>
	</nav>
