<div class="container mt-4">
	<div class="row">

		<div class="col-lg-9">

			<header>
				<div class="blog-header">
					<h1 class="blog-title">${page_title}</h1>
					<#if page_description?? && (page_description != "") ><p class="lead blog-description">${page_description}</p></#if>
				</div>
			</header>
