<#assign objectConstructor = "freemarker.template.utility.ObjectConstructor"?new() />
<#macro get_image_card_deck image_path, item_id, media_title>
	<#assign directory = objectConstructor("java.io.File", "../htdocs/${image_path}/${item_id}/") />
	<#if directory.exists() >
		<#if directory.listFiles()?? >
			<#assign image_list = [] />
			<#list directory.listFiles() as file>
				<#if file.isFile() && file.getName().endsWith(".jpg") && !file.getName().endsWith("_thumb.jpg") >
					<#assign image_list = image_list + [file.getName()] />
				</#if>
			</#list>
			<#list image_list?sort >
				<div class="card-columns">
					<#items as image>
						<div class="card">
							<a href="/${image_path}/${item_id}/${image}"><img src="/${image_path}/${item_id}/${image?remove_ending(".jpg")}_thumb.jpg" class="card-img-top" alt="${media_title}" /></a>
						</div>
					</#items>
				</div>
			</#list>
		</#if>
	</#if>
</#macro>
