			</div>
		</div>

		<footer id="footer">
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
						<hr />
						<p>
							<#-- use <#setting locale="de" /> for setting locale but this disturbs the output of numbers (lat and lon), therefore switched off for now -->
							Letzte Änderung: <time datetime="${.now?date?string["yyyy-MM-dd"]}">${.now?date?string["EEEE, d. MMMM yyyy"]}</time>
							&bull;
							<span class='fa fa-hand-rock-o'></span> <span class='fa fa-hand-paper-o'></span> <span class='fa fa-hand-scissors-o'></span> <span class='fa fa-hand-lizard-o'></span> <span class='fa fa-hand-spock-o'></span>
							&bull;
							<a href="/imprint_${lang}.html"><span class='fa fa-balance-scale'></span> Impressum</a>
						</p>
					</div>
				</div>
			</div>
		</footer>

	</body>

</html>
