<!DOCTYPE html>
<html lang="${lang}">
	<head>

		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />

		<meta name="description" content="Cinemap - eine Karte von Orten, an denen Filme gedreht wurden oder Bücher spielen." />
		<meta name="author" content="Ekkart Kleinod" />

		<title>${page_title} | Cinemap</title>

		<link href="/images/icon.png" rel="icon" type="image/png" />

		<link href="/css/bootstrap-themes/yeti/bootstrap.min.css" rel="stylesheet" type="text/css" />
		<link href="/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
		<link href="/css/cinemap.css" rel="stylesheet" type="text/css" />

		<script src="/js/jquery.min.js"></script>
		<script src="/js/bootstrap.min.js"></script>

	</head>
