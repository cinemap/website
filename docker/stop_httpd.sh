#!/bin/bash

# show running containers: sudo docker ps
# show all containers: sudo docker ps --all

# stop
sudo docker stop cinemap.de

# remove
sudo docker rm cinemap.de
