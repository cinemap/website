#!/bin/bash

# prerequisites:
# install docker: https://docs.docker.com/install/linux/docker-ce/ubuntu/
# install https from docker: sudo docker pull httpd

sudo docker run --detach --interactive --tty --name cinemap.de --publish 8080:80 --volume "$PWD/../htdocs/":/usr/local/apache2/htdocs/ httpd:2.4
